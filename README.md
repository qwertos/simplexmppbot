

#XMPP Bot

##Dependencies

+	ruby
+	xmpp4r ruby gem

###Install

####Fedora/RHEL

	yum install ruby rubygem-xmpp4r

####Ubuntu

	apt-get install ruby rubygems
	gem install xmpp4r

##Install

	cd /usr/local/bin

##Config

	mkdir -p /etc/xmppbot/
	cp config.rb.example /etc/xmppbot/config.rb

Add information to config.rb file.




